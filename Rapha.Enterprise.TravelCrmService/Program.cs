﻿//----------------------------------------------
// <copyright file="Program.cs" company="Rapha">
//     Copyright (c) Rapha. All rights reserved.
// </copyright>
//----------------------------------------------
namespace Rapha.Enterprise.TravelCrmService
{
    using System;
    using System.ServiceProcess;

    /// <summary>
    /// The application start-up class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main()
        {
            ServiceBase[] servicesToRun;

            servicesToRun = new ServiceBase[] 
            { 
                new OrderProcessService() 
            };
            
            ServiceBase.Run(servicesToRun);
        }
    }
}
