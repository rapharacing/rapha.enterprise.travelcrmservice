﻿//-------------------------------------------------------
// <copyright file="ProjectInstaller.cs" company="Rapha">
//     Copyright (c) Rapha. All rights reserved.
// </copyright>
//-------------------------------------------------------
namespace Rapha.Enterprise.TravelCrmService
{
    using System;
    using System.ComponentModel;
    using System.Configuration.Install;

    /// <summary>
    /// The project installer class.
    /// </summary>
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectInstaller"/> class.
        /// </summary>
        public ProjectInstaller()
        {
            this.InitializeComponent();
        }
    }
}
