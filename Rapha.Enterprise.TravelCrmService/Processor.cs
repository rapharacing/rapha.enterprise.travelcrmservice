﻿//------------------------------------------------
// <copyright file="Processor.cs" company="Rapha">
//     Copyright (c) Rapha. All rights reserved.
// </copyright>
//------------------------------------------------
namespace Rapha.Enterprise.TravelCrmService
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.IO;
    using System.Linq;
    using System.Security;
    using System.Xml;

    using Rapha.Enterprise.ServicesWrapper;
    using Rapha.Enterprise.ServicesWrapper.Model.Insightly;

    /// <summary>
    /// The class handling the reading and processing of files in the given directory.
    /// </summary>
    public class Processor
    {
        #region Member variables

        /// <summary>
        /// The API URL.
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// The API key.
        /// </summary>
        private string apiKey;

        /// <summary>
        /// The processing file directory.
        /// </summary>
        private string directory;

        /// <summary>
        /// The earliest file modified date and time to process.
        /// </summary>
        private DateTime startDateTime;

        /// <summary>
        /// The custom field name in a project for the deposit SKU.
        /// </summary>
        private string skuDepositField;

        /// <summary>
        /// The custom field name in a project for the balance SKU.
        /// </summary>
        private string skuBalanceField;

        /// <summary>
        /// The character used as delimiter in configuration lists.
        /// </summary>
        private char delimiterCharacter;

        /// <summary>
        /// The error messages.
        /// </summary>
        private StringCollection errorMessages;

        /// <summary>
        /// The list of IDs of orders already processed.
        /// </summary>
        private StringCollection processedOrders;

        /// <summary>
        /// The customer user group names that should be added as tags on a contact.
        /// </summary>
        private StringCollection contactTags = null;

        /// <summary>
        /// The list of projects.
        /// </summary>
        private List<Project> projects = null;

        #endregion

        #region Public methods

        /// <summary>
        /// Processes all the travel orders in the directory modified since the last processing time.
        /// </summary>
        /// <returns>A list of error messages.</returns>
        public StringCollection ProcessOrders()
        {
            this.errorMessages = new StringCollection();

            this.GetConfigSettings();

            if (this.errorMessages.Count > 0)
            {
                return this.errorMessages;
            }

            this.GetProjects();

            if (this.errorMessages.Count > 0)
            {
                return this.errorMessages;
            }

            if (this.projects == null || this.projects.Count == 0)
            {
                this.errorMessages.Add(Constants.NoProjectsMessage);
                return this.errorMessages;
            }

            var lastDateTime = this.ProcessFiles(this.directory, this.startDateTime);

            if (lastDateTime > this.startDateTime)
            {
                Properties.Settings.Default.LastDateTime = lastDateTime;
                Properties.Settings.Default.Save();
            }

            return this.errorMessages;
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Gets the configuration settings.
        /// </summary>
        private void GetConfigSettings()
        {
            this.apiUrl = Properties.Settings.Default.ApiUrl;
            this.apiKey = Properties.Settings.Default.ApiKey;
            this.directory = Properties.Settings.Default.ProcessDirectory;
            this.startDateTime = Properties.Settings.Default.LastDateTime;
            this.skuDepositField = Properties.Settings.Default.SkuDepositField;
            this.skuBalanceField = Properties.Settings.Default.SkuBalanceField;
            var delimiterString = Properties.Settings.Default.Delimiter;
            var contactTagsString = Properties.Settings.Default.ContactTags;

            if (string.IsNullOrEmpty(this.apiUrl))
            {
                this.errorMessages.Add(string.Format(Constants.ConfigSettingMissingMessage, "ApiUrl"));
            }

            if (string.IsNullOrEmpty(this.apiKey))
            {
                this.errorMessages.Add(string.Format(Constants.ConfigSettingMissingMessage, "ApiKey"));
            }

            if (string.IsNullOrEmpty(this.directory))
            {
                this.errorMessages.Add(string.Format(Constants.ConfigSettingMissingMessage, "ProcessDirectory"));
            }

            if (this.startDateTime == null)
            {
                this.startDateTime = DateTime.MinValue;
            }

            if (string.IsNullOrEmpty(this.skuDepositField))
            {
                this.errorMessages.Add(string.Format(Constants.ConfigSettingMissingMessage, "SkuDepositField"));
            }

            if (string.IsNullOrEmpty(this.skuBalanceField))
            {
                this.errorMessages.Add(string.Format(Constants.ConfigSettingMissingMessage, "SkuBalanceField"));
            }

            if (string.IsNullOrEmpty(delimiterString))
            {
                this.delimiterCharacter = Constants.DefaultDelimiter;
            }
            else
            {
                this.delimiterCharacter = delimiterString.ToCharArray()[0];
            }

            if (!string.IsNullOrEmpty(contactTagsString))
            {
                var tags = contactTagsString.Split(this.delimiterCharacter);

                this.contactTags = new StringCollection();

                foreach (var tag in tags)
                {
                    this.contactTags.Add(tag.ToUpperInvariant());
                }
            }
        }

        /// <summary>
        /// Adds a new contact.
        /// </summary>
        /// <param name="email">The email address.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <param name="file">The full file path.</param>
        /// <returns>The contact.</returns>
        private Contact AddContact(string email, string firstName, string lastName, string file)
        {
            try
            {
                var callHandler = new ServiceCallHandler();
                var contact = callHandler.AddContact(this.apiUrl, this.apiKey, email, firstName, lastName);

                return contact;
            }
            catch (ApplicationException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "POST", Constants.ContactsApi, file, exception.Message));
            }
            catch (WebRequestException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "POST", Constants.ContactsApi, file, exception.Message));
            }
            catch (WebResponseException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "POST", Constants.ContactsApi, file, exception.Message));
            }

            return null;
        }

        /// <summary>
        /// Adds the project link to the contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <param name="projectId">The project ID.</param>
        /// <param name="file">The full file path.</param>
        /// <returns>The added contact.</returns>
        private Contact AddProjectLinkToContact(Contact contact, string projectId, string file)
        {
            try
            {
                var callHandler = new ServiceCallHandler();
                contact = callHandler.AddProjectLinkToContact(this.apiUrl, this.apiKey, contact, projectId);

                return contact;
            }
            catch (ApplicationException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "PUT", Constants.ContactsApi, file, exception.Message));
            }
            catch (WebRequestException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "PUT", Constants.ContactsApi, file, exception.Message));
            }
            catch (WebResponseException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "PUT", Constants.ContactsApi, file, exception.Message));
            }

            return contact;
        }

        /// <summary>
        /// Adds the tags to the contact.
        /// </summary>
        /// <param name="contact">The contact.</param>
        /// <param name="tags">The tags.</param>
        /// <param name="file">The full file path.</param>
        /// <returns>The updated contact.</returns>
        private Contact AddTagsToContact(Contact contact, StringCollection tags, string file)
        {
            try
            {
                var callHandler = new ServiceCallHandler();
                contact = callHandler.AddTagsToContact(this.apiUrl, this.apiKey, contact, tags);

                return contact;
            }
            catch (ApplicationException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "PUT", Constants.ContactsApi, file, exception.Message));
            }
            catch (WebRequestException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "PUT", Constants.ContactsApi, file, exception.Message));
            }
            catch (WebResponseException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "PUT", Constants.ContactsApi, file, exception.Message));
            }

            return contact;
        }

        /// <summary>
        /// Adds the note to the contact.
        /// </summary>
        /// <param name="title">The note title.</param>
        /// <param name="sku">The SKU.</param>
        /// <param name="orderDate">The order date.</param>
        /// <param name="orderNumber">The order number.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="currency">The currency.</param>
        /// <param name="contactId">The contact ID.</param>
        /// <param name="file">The full file path.</param>
        /// <returns>The note.</returns>
        private Note AddNoteToContact(string title, string sku, string orderDate, string orderNumber, string amount, string currency, string contactId, string file)
        {
            string content = string.Format(Constants.PaymentNoteBodyText, sku, orderDate, orderNumber, amount, currency);

            try
            {
                var callHandler = new ServiceCallHandler();
                var note = callHandler.AddNote(this.apiUrl, this.apiKey, title, content, contactId, Constants.ContactLinkType);

                return note;
            }
            catch (ApplicationException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "POST", Constants.NotesApi, file, exception.Message));
            }
            catch (WebRequestException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "POST", Constants.NotesApi, file, exception.Message));
            }
            catch (WebResponseException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "POST", Constants.NotesApi, file, exception.Message));
            }

            return null;
        }

        /// <summary>
        /// Gets all the projects.
        /// </summary>
        private void GetProjects()
        {
            try
            {
                var callHandler = new ServiceCallHandler();
                this.projects = callHandler.GetProjects(this.apiUrl, this.apiKey);
            }
            catch (ApplicationException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiCallFailedMessage, "GET", Constants.ProjectsApi, exception.Message));
            }
            catch (WebRequestException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiCallFailedMessage, "GET", Constants.ProjectsApi, exception.Message));
            }
            catch (WebResponseException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiCallFailedMessage, "GET", Constants.ProjectsApi, exception.Message));
            }
        }

        /// <summary>
        /// Gets the contact by email address.
        /// </summary>
        /// <param name="email">The email address.</param>
        /// <param name="file">The full file path.</param>
        /// <returns>The contact.</returns>
        private Contact GetContactByEmail(string email, string file)
        {
            List<Contact> contacts = null;

            try
            {
                var callHandler = new ServiceCallHandler();
                contacts = callHandler.GetContactsByEmail(this.apiUrl, this.apiKey, email);
            }
            catch (ApplicationException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "GET", Constants.ContactsApi, file, exception.Message));
            }
            catch (WebRequestException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "GET", Constants.ContactsApi, file, exception.Message));
            }
            catch (WebResponseException exception)
            {
                this.errorMessages.Add(string.Format(Constants.ApiFileCallFailedMessage, "GET", Constants.ContactsApi, file, exception.Message));
            }

            if (contacts != null && contacts.Count > 0)
            {
                return contacts[0];
            }

            return null;
        }

        /// <summary>
        /// Processes the files in the given directory.
        /// </summary>
        /// <param name="directory">The directory.</param>
        /// <param name="startDateTime">The earliest file modification date and time.</param>
        /// <returns>The latest file modification date and time.</returns>
        private DateTime ProcessFiles(string directory, DateTime startDateTime)
        {
            var lastDateTime = startDateTime;

            if (!Directory.Exists(directory))
            {
                this.errorMessages.Add(string.Format(Constants.DirectoryNotFoundMessage, directory));
                return lastDateTime;
            }

            IEnumerable<string> files = null;

            try
            {
                files = from file in Directory.EnumerateFiles(directory)
                        where file.ToUpperInvariant().EndsWith(".XML")
                        select file;
            }
            catch (ArgumentException exception)
            {
                this.errorMessages.Add(string.Format(Constants.DirectoryNotAccessibleMessage, directory, exception.Message));
            }
            catch (PathTooLongException exception)
            {
                this.errorMessages.Add(string.Format(Constants.DirectoryNotAccessibleMessage, directory, exception.Message));
            }
            catch (SecurityException exception)
            {
                this.errorMessages.Add(string.Format(Constants.DirectoryNotAccessibleMessage, directory, exception.Message));
            }
            catch (UnauthorizedAccessException exception)
            {
                this.errorMessages.Add(string.Format(Constants.DirectoryNotAccessibleMessage, directory, exception.Message));
            }

            if (files == null)
            {
                return lastDateTime;
            }

            this.processedOrders = new StringCollection();

            foreach (var file in files)
            {
                try
                {
                    var fileDateTime = File.GetLastWriteTime(file);

                    if (fileDateTime > startDateTime)
                    {
                        this.ReadFile(file);

                        if (fileDateTime > lastDateTime)
                        {
                            lastDateTime = fileDateTime;
                        }
                    }
                }
                catch (ArgumentException exception)
                {
                    this.errorMessages.Add(string.Format(Constants.FileUnavailableMessage, file, exception.Message));
                }
                catch (IOException exception)
                {
                    this.errorMessages.Add(string.Format(Constants.FileUnavailableMessage, file, exception.Message));
                }
                catch (NotSupportedException exception)
                {
                    this.errorMessages.Add(string.Format(Constants.FileUnavailableMessage, file, exception.Message));
                }
                catch (SecurityException exception)
                {
                    this.errorMessages.Add(string.Format(Constants.FileUnavailableMessage, file, exception.Message));
                }
                catch (UnauthorizedAccessException exception)
                {
                    this.errorMessages.Add(string.Format(Constants.FileUnavailableMessage, file, exception.Message));
                }
            }

            return lastDateTime;
        }

        /// <summary>
        /// Reads and processes the file.
        /// </summary>
        /// <param name="file">The full file path.</param>
        private void ReadFile(string file)
        {
            var xml = File.ReadAllText(file);
            var document = new XmlDocument();

            try
            {
                document.LoadXml(xml);
            }
            catch (XmlException exception)
            {
                this.errorMessages.Add(string.Format(Constants.FileContentsNotValidMessage, file, exception.Message));
                return;
            }

            var node = document.SelectSingleNode("descendant::" + Constants.OrderTypeNode);

            if (node != null && node.InnerText.ToUpperInvariant() == Constants.TravelTypeText)
            {
                this.ProcessTravelFile(document, file);
            }
        }

        /// <summary>
        /// Processes the travel file.
        /// </summary>
        /// <param name="document">The XML document.</param>
        /// <param name="file">The full file path.</param>
        private void ProcessTravelFile(XmlDocument document, string file)
        {
            var orderIdNode = document.SelectSingleNode("descendant::" + Constants.OrderIdNode);

            if (orderIdNode == null)
            {
                this.errorMessages.Add(string.Format(Constants.OrderIdNodeNotFoundMessage, file));
            }
            else if (this.processedOrders.Contains(orderIdNode.InnerText))
            {
                return;
            }

            var userNode = document.SelectSingleNode("descendant::" + Constants.UserNode);

            if (userNode == null)
            {
                this.errorMessages.Add(string.Format(Constants.UserNodeNotFoundMessage, file));
            }

            var skuNodes = document.SelectNodes("descendant::" + Constants.SkuNode);

            if (skuNodes == null || skuNodes.Count == 0)
            {
                this.errorMessages.Add(string.Format(Constants.SkuNodeNotFoundMessage, file));
            }

            var firstNameNodes = document.SelectNodes("descendant::" + Constants.FirstNameNode);
            var lastNameNodes = document.SelectNodes("descendant::" + Constants.LastNameNode);

            var firstName = Constants.DefaultName;
            var lastName = Constants.DefaultName;

            if (firstNameNodes != null && firstNameNodes.Count > 0)
            {
                firstName = firstNameNodes[0].InnerText;
            }

            if (lastNameNodes != null && lastNameNodes.Count > 0)
            {
                lastName = lastNameNodes[0].InnerText;
            }

            var orderDate = string.Empty;
            var currency = string.Empty;
            var amount = string.Empty;

            var orderDateNode = document.SelectSingleNode("descendant::" + Constants.OrderDateTimeNode);

            if (orderDateNode != null && orderDateNode.InnerText.Length >= 10)
            {
                orderDate = orderDateNode.InnerText.Substring(0, 10);
            }
            else
            {
                this.errorMessages.Add(string.Format(Constants.OrderDateNodeNotFoundMessage, file));
            }

            var currencyNode = document.SelectSingleNode("descendant::" + Constants.CurrencyNode);

            if (currencyNode != null)
            {
                currency = currencyNode.InnerText;
            }
            else
            {
                this.errorMessages.Add(string.Format(Constants.CurrencyNodeNotFoundMessage, file));
            }

            var amountNode = document.SelectSingleNode("descendant::" + Constants.AmountNode);

            if (amountNode != null)
            {
                amount = amountNode.InnerText;
            }
            else
            {
                this.errorMessages.Add(string.Format(Constants.AmountNodeNotFoundMessage, file));
            }

            if (orderIdNode == null || userNode == null || skuNodes == null)
            {
                return;
            }

            StringCollection userGroups = null;

            if (this.contactTags != null && this.contactTags.Count > 0)
            {
                var userGroupNodes = document.SelectNodes("descendant::" + Constants.UserGroupNode);

                if (userGroupNodes != null && userGroupNodes.Count > 0)
                {
                    userGroups = new StringCollection();

                    foreach (XmlNode userGroupNode in userGroupNodes)
                    {
                        if (this.contactTags.Contains(userGroupNode.InnerText.ToUpperInvariant()))
                        {
                            userGroups.Add(userGroupNode.InnerText);
                        }
                    }
                }
            }

            foreach (XmlNode skuNode in skuNodes)
            {
                this.ProcessOrderLine(
                    orderIdNode.InnerText,
                    userNode.InnerText,
                    firstName,
                    lastName,
                    skuNode.InnerText,
                    orderDate,
                    currency,
                    amount,
                    userGroups,
                    file);
            }

            this.processedOrders.Add(orderIdNode.InnerText);
        }

        /// <summary>
        /// Processes the order line.
        /// </summary>
        /// <param name="orderNumber">The order number.</param>
        /// <param name="email">The contact email address.</param>
        /// <param name="firstName">The contact first name.</param>
        /// <param name="lastName">The contact last name.</param>
        /// <param name="sku">The SKU.</param>
        /// <param name="orderDate">The order date.</param>
        /// <param name="currency">The currency.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="userGroups">The contact user groups.</param>
        /// <param name="file">The full file path.</param>
        private void ProcessOrderLine(
            string orderNumber,
            string email,
            string firstName,
            string lastName,
            string sku,
            string orderDate,
            string currency,
            string amount,
            StringCollection userGroups,
            string file)
        {
            var projectFound = false;

            foreach (var project in this.projects)
            {
                if (project.CustomFields != null)
                {
                    foreach (var customField in project.CustomFields)
                    {
                        if (customField.CustomFieldName.ToUpperInvariant().StartsWith(this.skuDepositField.ToUpperInvariant())
                            && customField.CustomFieldValue.ToUpperInvariant() == sku.ToUpperInvariant())
                        {
                            this.ProcessOrder(orderNumber, email, firstName, lastName, orderDate, sku, currency, amount, Constants.DepositNoteTitle, project, userGroups, file);
                            projectFound = true;
                            break;
                        }

                        if (customField.CustomFieldName.ToUpperInvariant().StartsWith(this.skuBalanceField.ToUpperInvariant())
                            && customField.CustomFieldValue.ToUpperInvariant() == sku.ToUpperInvariant())
                        {
                            this.ProcessOrder(orderNumber, email, firstName, lastName, orderDate, sku, currency, amount, Constants.BalanceNoteTitle, project, userGroups, file);
                            projectFound = true;
                            break;
                        }
                    }
                }

                if (projectFound)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Processes the order.
        /// </summary>
        /// <param name="orderNumber">The order number.</param>
        /// <param name="email">The contact email address.</param>
        /// <param name="firstName">The contact first name.</param>
        /// <param name="lastName">The contact last name.</param>
        /// <param name="orderDate">The order date.</param>
        /// <param name="sku">The SKU.</param>
        /// <param name="currency">The currency.</param>
        /// <param name="amount">The amount.</param>
        /// <param name="noteTitle">The note title.</param>
        /// <param name="project">The project.</param>
        /// <param name="userGroups">The contact user groups.</param>
        /// <param name="file">The full file path.</param>
        private void ProcessOrder(
            string orderNumber,
            string email,
            string firstName,
            string lastName,
            string orderDate,
            string sku,
            string currency,
            string amount,
            string noteTitle,
            Project project,
            StringCollection userGroups,
            string file)
        {
            var contact = this.GetContactByEmail(email, file);

            if (contact == null)
            {
                contact = this.AddContact(email, firstName, lastName, file);
            }

            if (contact == null)
            {
                this.errorMessages.Add(string.Format(Constants.ContactNotAddedMessage, file, firstName, lastName, email));
                return;
            }

            var contactId = contact.ContactId;

            contact = this.AddProjectLinkToContact(contact, project.ProjectId, file);
            contact = this.AddTagsToContact(contact, userGroups, file);
            var note = this.AddNoteToContact(Constants.DepositNoteTitle, sku, orderDate, orderNumber, amount, currency, contactId, file);
        }

        #endregion
    }
}