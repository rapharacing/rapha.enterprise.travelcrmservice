﻿//-------------------------------------------------------
// <copyright file="ProjectInstaller.Designer.cs" company="Rapha">
//     Copyright (c) Rapha. All rights reserved.
// </copyright>
//-------------------------------------------------------

namespace Rapha.Enterprise.TravelCrmService
{
    /// <summary>
    /// The project installer class.
    /// </summary>
    public partial class ProjectInstaller
    {
        /// <summary>
        /// The order service process installer.
        /// </summary>
        private System.ServiceProcess.ServiceProcessInstaller orderServiceProcessInstaller;

        /// <summary>
        /// The order service installer.
        /// </summary>
        private System.ServiceProcess.ServiceInstaller orderServiceInstaller;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.orderServiceProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.orderServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // orderServiceProcessInstaller
            // 
            this.orderServiceProcessInstaller.Password = null;
            this.orderServiceProcessInstaller.Username = null;
            // 
            // orderServiceInstaller
            // 
            this.orderServiceInstaller.Description = "Travel order processing";
            this.orderServiceInstaller.DisplayName = "Travel Order Service";
            this.orderServiceInstaller.ServiceName = "OrderProcessService";
            this.orderServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.orderServiceProcessInstaller,
            this.orderServiceInstaller});

        }

        #endregion
    }
}