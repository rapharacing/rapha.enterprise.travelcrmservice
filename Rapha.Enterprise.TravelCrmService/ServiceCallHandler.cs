﻿//---------------------------------------------------------
// <copyright file="ServiceCallHandler.cs" company="Rapha">
//     Copyright (c) Rapha. All rights reserved.
// </copyright>
//---------------------------------------------------------
namespace Rapha.Enterprise.TravelCrmService
{
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Serialization;

    using Rapha.Enterprise.ServicesWrapper;
    using Rapha.Enterprise.ServicesWrapper.Model.Insightly;

    /// <summary>
    /// The class handling the calls to the service APIs.
    /// </summary>
    public class ServiceCallHandler
    {
        #region Public methods

        /// <summary>
        /// Adds a new contact.
        /// </summary>
        /// <param name="apiUrl">The API URL.</param>
        /// <param name="apiKey">The API key.</param>
        /// <param name="email">The email address.</param>
        /// <param name="firstName">The first name.</param>
        /// <param name="lastName">The last name.</param>
        /// <returns>The contact</returns>
        public Contact AddContact(string apiUrl, string apiKey, string email, string firstName, string lastName)
        {
            var uri = new Uri(new Uri(apiUrl), Constants.ContactsApi);
            var url = uri.ToString();

            var builder = new StringBuilder();

            builder.Append("<Contact>");
            builder.Append("<FirstName>");
            builder.Append(firstName);
            builder.Append("</FirstName>");
            builder.Append("<LastName>");
            builder.Append(lastName);
            builder.Append("</LastName>");
            builder.Append("<ContactInfos>");
            builder.Append("<ContactInfo>");
            builder.Append("<ContactInfoId />");
            builder.Append("<ContactInfoType>EMAIL</ContactInfoType>");
            builder.Append("<Detail>");
            builder.Append(email);
            builder.Append("</Detail>");
            builder.Append("</ContactInfo>");
            builder.Append("</ContactInfos>");
            builder.Append("</Contact>");

            var body = builder.ToString();

            var handler = new EncodingCallHandler();
            var xml = handler.GetResponse(url, "POST", apiKey, body, null, null);

            var deserialised = this.DeserialiseXml(xml) as Contact;

            if (deserialised != null)
            {
                return deserialised;
            }

            return null;
        }

        /// <summary>
        /// Adds a note.
        /// </summary>
        /// <param name="apiUrl">The API URL.</param>
        /// <param name="apiKey">The API key.</param>
        /// <param name="title">The note title.</param>
        /// <param name="content">The note content.</param>
        /// <param name="subjectId">The subject ID.</param>
        /// <param name="subjectType">The type of subject.</param>
        /// <returns>The note.</returns>
        public Note AddNote(string apiUrl, string apiKey, string title, string content, string subjectId, string subjectType)
        {
            var uri = new Uri(new Uri(apiUrl), Constants.NotesApi);
            var url = uri.ToString();

            var builder = new StringBuilder();

            builder.Append("<Note>");
            builder.Append("<Title>");
            builder.Append(title);
            builder.Append("</Title>");
            builder.Append("<Body>");
            builder.Append(content);
            builder.Append("</Body>");
            builder.Append("<LinkSubjectId>");
            builder.Append(subjectId);
            builder.Append("</LinkSubjectId>");
            builder.Append("<LinkSubjectType>");
            builder.Append(subjectType);
            builder.Append("</LinkSubjectType>");
            builder.Append("</Note>");

            var body = builder.ToString();

            var handler = new EncodingCallHandler();
            var xml = handler.GetResponse(url, "POST", apiKey, body, null, null);

            var deserialised = this.DeserialiseXml(xml) as Note;

            if (deserialised != null)
            {
                return deserialised;
            }

            return null;
        }

        /// <summary>
        /// Adds the project link to the contact.
        /// </summary>
        /// <param name="apiUrl">The API URL.</param>
        /// <param name="apiKey">The API key.</param>
        /// <param name="contact">The contact.</param>
        /// <param name="projectId">The project ID.</param>
        /// <returns>The updated contact.</returns>
        public Contact AddProjectLinkToContact(string apiUrl, string apiKey, Contact contact, string projectId)
        {
            if (contact.Links != null && contact.Links.Length > 0)
            {
                var contactLinks = (from contactLink in contact.Links
                                    where contactLink.FirstProjectId == projectId
                                    select contactLink).ToList<Link>();

                if (contactLinks.Count > 0)
                {
                    return contact;
                }
            }

            var uri = new Uri(new Uri(apiUrl), Constants.ContactsApi);
            var url = uri.ToString();

            var link = new Link();
            var links = new List<Link>();

            if (contact.Links != null)
            {
                links = contact.Links.ToList<Link>();
            }

            link.FirstProjectId = projectId;
            links.Add(link);

            contact.Links = links.ToArray();
            contact = this.UpdateContact(apiUrl, apiKey, contact);

            return contact;
        }

        /// <summary>
        /// Adds the tags to the contact.
        /// </summary>
        /// <param name="apiUrl">The API URL.</param>
        /// <param name="apiKey">The API key.</param>
        /// <param name="contact">The contact.</param>
        /// <param name="tags">The tags.</param>
        /// <returns>The updated contact.</returns>
        public Contact AddTagsToContact(string apiUrl, string apiKey, Contact contact, StringCollection tags)
        {
            if (tags == null || tags.Count == 0)
            {
                return contact;
            }

            var allTags = new List<Tag>();
            var tagAdded = false;

            if (contact.Tags != null && contact.Tags.Length > 0)
            {
                allTags = contact.Tags.ToList<Tag>();

                foreach (var tag in tags)
                {
                    var matchedTags = (from contactTag in contact.Tags
                                       where contactTag.TagName.ToUpperInvariant() == tag.ToUpperInvariant()
                                       select contactTag).ToList<Tag>();

                    if (matchedTags == null || matchedTags.Count == 0)
                    {
                        var newTag = new Tag();
                        newTag.TagName = tag;
                        allTags.Add(newTag);
                        tagAdded = true;
                    }
                }
            }
            else
            {
                foreach (var tag in tags)
                {
                    var newTag = new Tag();
                    newTag.TagName = tag;
                    allTags.Add(newTag);
                    tagAdded = true;
                }
            }

            if (tagAdded)
            {
                var uri = new Uri(new Uri(apiUrl), Constants.ContactsApi);
                var url = uri.ToString();

                contact.Tags = allTags.ToArray();
                contact = this.UpdateContact(apiUrl, apiKey, contact);
            }

            return contact;
        }

        /// <summary>
        /// Gets all the projects.
        /// </summary>
        /// <param name="apiUrl">The API URL.</param>
        /// <param name="apiKey">The API key.</param>
        /// <returns>The list of projects.</returns>
        public List<Project> GetProjects(string apiUrl, string apiKey)
        {
            var uri = new Uri(new Uri(apiUrl), Constants.ProjectsApi);
            var url = uri.ToString();

            var handler = new EncodingCallHandler();
            var xml = handler.GetResponse(url, "GET", apiKey, null, null, null);

            var deserialised = this.DeserialiseXml(xml) as ProjectCollection;

            if (deserialised != null && deserialised.Projects != null && deserialised.Projects.Length > 0)
            {
                return deserialised.Projects.ToList<Project>();
            }

            return new List<Project>();
        }

        /// <summary>
        /// Gets the contact by email address.
        /// </summary>
        /// <param name="apiUrl">The API URL.</param>
        /// <param name="apiKey">The API key.</param>
        /// <param name="email">The email address.</param>
        /// <returns>The list of contacts.</returns>
        public List<Contact> GetContactsByEmail(string apiUrl, string apiKey, string email)
        {
            var uri = new Uri(new Uri(apiUrl), Constants.ContactsApi);
            var url = uri.ToString();

            var arguments = string.Format(CultureInfo.InvariantCulture, @"{0}={1}", Constants.EmailArgument, email);

            var handler = new EncodingCallHandler();
            var xml = handler.GetResponse(url, "GET", apiKey, null, arguments, null);

            if (string.IsNullOrEmpty(xml))
            {
                return new List<Contact>();
            }

            var deserialised = this.DeserialiseXml(xml) as ContactCollection;

            if (deserialised != null && deserialised.Contacts != null && deserialised.Contacts.Length > 0)
            {
                return deserialised.Contacts.ToList<Contact>();
            }

            return new List<Contact>();
        }

        #endregion

        #region Private methods

        /// <summary>
        /// Deserialises the XML into a DataContract implemented object.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <returns>The deserialised object.</returns>
        /// <exception cref="System.ApplicationException">Thrown if the XML cannot be deserialised.</exception>
        private object DeserialiseXml(string xml)
        {
            var root = this.GetXmlRootNode(xml);
            var type = this.GetTypeFromXml(root);

            xml = this.AddCollectionRootToXml(xml, root);

            var serialiser = new XmlSerializer(type);
            var stringReader = new StringReader(xml);

            try
            {
                var result = serialiser.Deserialize(stringReader);
                return result;
            }
            catch (InvalidOperationException exception)
            {
                throw new ApplicationException(Constants.XmlInvalidMessage, exception);
            }
        }

        /// <summary>
        /// Serialises a DataContract implemented object into XML.
        /// </summary>
        /// <param name="serialisable">The serialisable object.</param>
        /// <returns>The XML.</returns>
        /// <exception cref="System.ApplicationException">Thrown if the object is not serialisable.</exception>
        private string SerialiseXml(object serialisable)
        {
            if (serialisable == null)
            {
                return string.Empty;
            }

            var serialiser = new XmlSerializer(serialisable.GetType());
            var builder = new StringBuilder();
            var settings = new XmlWriterSettings();
            var namespaces = new XmlSerializerNamespaces();

            settings.OmitXmlDeclaration = true;
            namespaces.Add(string.Empty, string.Empty);

            using (var writer = XmlWriter.Create(builder, settings))
            {
                try
                {
                    serialiser.Serialize(writer, serialisable, namespaces);
                    return builder.ToString();
                }
                catch (InvalidOperationException exception)
                {
                    throw new ApplicationException(string.Format(CultureInfo.InvariantCulture, Constants.ObjectNotSerializableMessage, serialisable.GetType().ToString()), exception);
                }
            }
        }

        /// <summary>
        /// Gets the object type from the XML.
        /// </summary>
        /// <param name="root">The XML root.</param>
        /// <returns>The type.</returns>
        /// <exception cref="System.ApplicationException">Thrown if the XML is for an unhandled type.</exception>
        private Type GetTypeFromXml(string root)
        {
            switch (root.ToUpperInvariant())
            {
                case "CONTACT":
                    return typeof(Contact);
                case "CONTACTS":
                    return typeof(ContactCollection);
                case "NOTE":
                    return typeof(Note);
                case "PROJECTS":
                    return typeof(ProjectCollection);
                default:
                    throw new ApplicationException(string.Format(CultureInfo.CurrentCulture, Constants.ObjectUnknownMessage, root));
            }
        }

        /// <summary>
        /// Adds the collection root to XML.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <param name="root">The XML root.</param>
        /// <returns>The extended XML.</returns>
        private string AddCollectionRootToXml(string xml, string root)
        {
            StringBuilder builder = new StringBuilder();

            switch (root.ToUpperInvariant())
            {
                case "CONTACTS":
                    builder.Append("<ContactCollection>");
                    builder.Append(xml);
                    builder.Append("</ContactCollection>");
                    break;
                case "PROJECTS":
                    builder.Append("<ProjectCollection>");
                    builder.Append(xml);
                    builder.Append("</ProjectCollection>");
                    break;
                default:
                    builder.Append(xml);
                    break;
            }

            return builder.ToString();
        }

        /// <summary>
        /// Gets the XML root node.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <returns>The XML root.</returns>
        /// <exception cref="System.ApplicationException">Thrown if the XML does not have a root node.</exception>
        private string GetXmlRootNode(string xml)
        {
            if (string.IsNullOrEmpty(xml))
            {
                throw new ApplicationException(Constants.XmlInvalidMessage);
            }

            xml = this.RemoveDocumentDeclaration(xml);

            var document = new XmlDocument();

            try
            {
                document.LoadXml(xml);
            }
            catch (XmlException exception)
            {
                throw new ApplicationException(Constants.XmlInvalidMessage, exception);
            }

            if (document.HasChildNodes)
            {
                return document.FirstChild.Name;
            }
            else
            {
                throw new ApplicationException(Constants.XmlInvalidMessage);
            }
        }

        /// <summary>
        /// Removes the document declaration from XML.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <returns>The XML with the declaration node removed.</returns>
        private string RemoveDocumentDeclaration(string xml)
        {
            var document = new XmlDocument();

            try
            {
                document.LoadXml(xml);
            }
            catch (XmlException)
            {
                // The input is not XML so there is no declaration to remove.
                return xml;
            }

            var node = document.FirstChild as XmlDeclaration;

            if (node != null)
            {
                document.RemoveChild(node);
            }

            return document.OuterXml;
        }

        /// <summary>
        /// Updates the contact.
        /// </summary>
        /// <param name="apiUrl">The API URL.</param>
        /// <param name="apiKey">The API key.</param>
        /// <param name="contact">The contact.</param>
        /// <returns>The updated contact.</returns>
        private Contact UpdateContact(string apiUrl, string apiKey, Contact contact)
        {
            var uri = new Uri(new Uri(apiUrl), Constants.ContactsApi);
            var url = uri.ToString();

            var body = this.SerialiseXml(contact);

            var handler = new EncodingCallHandler();
            var xml = handler.GetResponse(url, "PUT", apiKey, body, null, null);

            var deserialised = this.DeserialiseXml(xml) as Contact;

            if (deserialised != null)
            {
                return deserialised;
            }

            return contact;
        }

        #endregion
    }
}
