﻿//----------------------------------------------------------
// <copyright file="OrderProcessService.cs" company="Rapha">
//     Copyright (c) Rapha. All rights reserved.
// </copyright>
//----------------------------------------------------------
namespace Rapha.Enterprise.TravelCrmService
{
    using System;
    using System.Diagnostics;
    using System.ServiceProcess;
    using System.Timers;

    /// <summary>
    /// The class handling the service events.
    /// </summary>
    public partial class OrderProcessService : ServiceBase
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="OrderProcessService" /> class.
        /// </summary>
        public OrderProcessService()
        {
            this.InitializeComponent();

            this.customEventLog = new System.Diagnostics.EventLog();

            if (!System.Diagnostics.EventLog.SourceExists("TravelCrmService"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "TravelCrmService", "Travel CRM Log");
            }

            this.customEventLog.Source = "TravelCrmService";
            this.customEventLog.Log = "Travel CRM Log";
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Start command is sent to the service by the Service Control Manager (SCM) or when 
        /// the operating system starts (for a service that starts automatically). Specifies actions to take when the service starts.
        /// </summary>
        /// <param name="args">Data passed by the start command.</param>
        protected override void OnStart(string[] args)
        {
            this.customEventLog.WriteEntry("Service starting");

            Timer timer = new Timer();
            timer.Interval = 900000;

            var interval = Properties.Settings.Default.TimerInterval;

            if (interval > 0)
            {
                timer.Interval = interval;
            }

            timer.Elapsed += new ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        /// <summary>
        /// Called each time the set timer interval has elapsed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ElapsedEventArgs"/> instance containing the event data.</param>
        protected void OnTimer(object sender, ElapsedEventArgs e)
        {
            var processor = new Processor();
            var errors = processor.ProcessOrders();

            if (errors != null && errors.Count > 0)
            {
                foreach (var error in errors)
                {
                    this.customEventLog.WriteEntry(error, EventLogEntryType.Error);
                }
            }
        }

        /// <summary>
        /// When implemented in a derived class, executes when a Stop command is sent to the service by the Service Control Manager (SCM). Specifies 
        /// actions to take when a service stops running.
        /// </summary>
        protected override void OnStop()
        {
            this.customEventLog.WriteEntry("Service stopping");
        }
    }
}
