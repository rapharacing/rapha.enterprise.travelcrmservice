﻿//-------------------------------------------------------------------
// <copyright file="OrderProcessService.Designer.cs" company="Rapha">
//     Copyright (c) Rapha. All rights reserved.
// </copyright>
//-------------------------------------------------------------------
namespace Rapha.Enterprise.TravelCrmService
{
    /// <summary>
    /// The class handling the service events.
    /// </summary>
    public partial class OrderProcessService
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// The custom event log.
        /// </summary>
        private System.Diagnostics.EventLog customEventLog;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">This is true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.customEventLog = new System.Diagnostics.EventLog();
            ((System.ComponentModel.ISupportInitialize)(this.customEventLog)).BeginInit();
            // 
            // OrderProcessService
            // 
            this.AutoLog = false;
            this.ServiceName = "OrderProcessService";
            ((System.ComponentModel.ISupportInitialize)(this.customEventLog)).EndInit();

        }

        #endregion
    }
}
