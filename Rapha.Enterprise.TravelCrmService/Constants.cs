﻿//------------------------------------------------
// <copyright file="Constants.cs" company="Rapha">
//     Copyright (c) Rapha. All rights reserved.
// </copyright>
//------------------------------------------------
namespace Rapha.Enterprise.TravelCrmService
{
    using System;

    /// <summary>
    /// The class holding all the constants.
    /// </summary>
    public class Constants
    {
        #region API constants

        /// <summary>
        /// The contact link type.
        /// </summary>
        public const string ContactLinkType = "Contact";

        #endregion

        #region API URLs

        /// <summary>
        /// The contacts API resource name.
        /// </summary>
        public const string ContactsApi = "contacts";

        /// <summary>
        /// The email argument name.
        /// </summary>
        public const string EmailArgument = "email";

        /// <summary>
        /// The notes API resource name.
        /// </summary>
        public const string NotesApi = "notes";

        /// <summary>
        /// The projects API resource name.
        /// </summary>
        public const string ProjectsApi = "projects";

        #endregion

        #region Error messages

        /// <summary>
        /// The error message used when the amount node is not found .
        /// </summary>
        public const string AmountNodeNotFoundMessage = "No Total Price can be found for file {0}.";

        /// <summary>
        /// The error message used when the API call fails.
        /// </summary>
        public const string ApiCallFailedMessage = "The call to the {0} {1} API failed. Error: {2}";

        /// <summary>
        /// The error message used when the API call fails and a file name is available.
        /// </summary>
        public const string ApiFileCallFailedMessage = "The call to the {0} {1} API for file {2} failed. Error: {3}";

        /// <summary>
        /// The error message used when the configuration setting is missing.
        /// </summary>
        public const string ConfigSettingMissingMessage = "The configuration setting {0} is missing.";

        /// <summary>
        /// The error message used when the contact cannot be added.
        /// </summary>
        public const string ContactNotAddedMessage = "File {0}: The contact {1} {2} with email address {3} cannot be added.";

        /// <summary>
        /// The error message used when the currency node is not found.
        /// </summary>
        public const string CurrencyNodeNotFoundMessage = "No Currency can be found for file {0}.";

        /// <summary>
        /// The error message used when the directory is not accessible.
        /// </summary>
        public const string DirectoryNotAccessibleMessage = "The directory {0} cannot be accessed. Error: {1}";

        /// <summary>
        /// The error message used when the directory is not found.
        /// </summary>
        public const string DirectoryNotFoundMessage = "The directory {0} cannot be found.";

        /// <summary>
        /// The error message used when the file contents are not valid XML.
        /// </summary>
        public const string FileContentsNotValidMessage = "The content of file {0} is not valid XML. Error: {1}";

        /// <summary>
        /// The error message used when the file is unavailable.
        /// </summary>
        public const string FileUnavailableMessage = "The file {0} cannot be read. Error: {1}";

        /// <summary>
        /// The error message used when no projects have been created.
        /// </summary>
        public const string NoProjectsMessage = "The orders cannot be processed because no projects could be found.";

        /// <summary>
        /// The error message used when the object is not a serializable type.
        /// </summary>
        public const string ObjectNotSerializableMessage = "The object of type {0} cannot be serialized.";

        /// <summary>
        /// The error message used when the object is of an unknown type.
        /// </summary>
        public const string ObjectUnknownMessage = "The XML root object {0} is not recognised.";

        /// <summary>
        /// The error message used when the order date node is not found.
        /// </summary>
        public const string OrderDateNodeNotFoundMessage = "No Order Creation Timestamp can be found for file {0}.";

        /// <summary>
        /// The error message used when the order ID node is not found.
        /// </summary>
        public const string OrderIdNodeNotFoundMessage = "No Order ID can be found for file {0}.";

        /// <summary>
        /// The error message used when the SKU node is not found.
        /// </summary>
        public const string SkuNodeNotFoundMessage = "No SKU can be found for file {0}.";

        /// <summary>
        /// The error message used when the user node is not found.
        /// </summary>
        public const string UserNodeNotFoundMessage = "No User can be found for file {0}.";

        /// <summary>
        /// The error message used when the XML returned from an API call is invalid.
        /// </summary>
        public const string XmlInvalidMessage = "The XML returned from an API call cannot be parsed.";

        #endregion

        #region Text fragments

        /// <summary>
        /// The title used for the balance paid notes.
        /// </summary>
        public const string BalanceNoteTitle = "Balance Paid";

        /// <summary>
        /// The character used as delimiter in configuration lists by default.
        /// </summary>
        public const char DefaultDelimiter = ';';

        /// <summary>
        /// The default name used when an actual name is not provided.
        /// </summary>
        public const string DefaultName = "Unknown";

        /// <summary>
        /// The title used for the deposit paid notes.
        /// </summary>
        public const string DepositNoteTitle = "Deposit Paid";

        /// <summary>
        /// The text used for the body in payment notes.
        /// </summary>
        public const string PaymentNoteBodyText = @"SKU:{0} &#xA; Order Date:{1} &#xA; Order No.:{2} &#xA; Amount:{3} &#xA; Currency:{4}";

        #endregion

        #region XML values

        /// <summary>
        /// The amount node XPath.
        /// </summary>
        public const string AmountNode = "order/totalPrice";

        /// <summary>
        /// The currency node XPath.
        /// </summary>
        public const string CurrencyNode = "order/currency";

        /// <summary>
        /// The first name node XPath.
        /// </summary>
        public const string FirstNameNode = "payment/paymentAddress/firstName";

        /// <summary>
        /// The last name node XPath.
        /// </summary>
        public const string LastNameNode = "payment/paymentAddress/lastName";

        /// <summary>
        /// The order date time node XPath.
        /// </summary>
        public const string OrderDateTimeNode = "orderCreationTimestamp";

        /// <summary>
        /// The order ID node XPath.
        /// </summary>
        public const string OrderIdNode = "order/id";

        /// <summary>
        /// The order type node XPath.
        /// </summary>
        public const string OrderTypeNode = "orderType";

        /// <summary>
        /// The SKU node XPath.
        /// </summary>
        public const string SkuNode = "lineItems/lineItem/sku";

        /// <summary>
        /// The travel order type text.
        /// </summary>
        public const string TravelTypeText = "TRAVEL";

        /// <summary>
        /// The user group node XPath.
        /// </summary>
        public const string UserGroupNode = "usergroups/usergroup/usergroupId";

        /// <summary>
        /// The user node XPath.
        /// </summary>
        public const string UserNode = "user";
        
        #endregion
    }
}
