﻿//--------------------------------------------------------------
// <copyright file="ServiceCallHandlerTests.cs" company="Rapha">
//     Copyright (c) Rapha. All rights reserved.
// </copyright>
//--------------------------------------------------------------
namespace Rapha.Enterprise.TravelCrmService.NUnitTest
{
    using System;
    using System.Collections.Specialized;

    using NUnit.Framework;

    /// <summary>
    /// The class containing the unit tests for the ServiceCallHandler class.
    /// </summary>
    [TestFixture]
    public class ServiceCallHandlerTests
    {
        /// <summary>
        /// Tests whether the get projects call is successful.
        /// </summary>
        [Test]
        public static void CanGetProjects()
        {
            var handler = new ServiceCallHandler();
            var response = handler.GetProjects(TestValues.InsightlyApiUrl, TestValues.InsightlyApiKey);

            Assert.IsNotNull(response);
        }

        /// <summary>
        /// Tests whether the get contacts by email call is successful.
        /// </summary>
        [Test]
        public static void CanGetContactsByEmail()
        {
            var handler = new ServiceCallHandler();
            var response = handler.GetContactsByEmail(TestValues.InsightlyApiUrl, TestValues.InsightlyApiKey, TestValues.Email);

            Assert.IsNotNull(response);
        }

        /// <summary>
        /// Tests whether the get contacts by email call when the email is not found is successful.
        /// </summary>
        [Test]
        public static void CanGetContactsByEmailNotFound()
        {
            var now = DateTime.Now;
            var handler = new ServiceCallHandler();
            var response = handler.GetContactsByEmail(TestValues.InsightlyApiUrl, TestValues.InsightlyApiKey, now.ToString("yyyyMMddHHmmss") + "@hotmail.com");

            Assert.IsNotNull(response);
            Assert.AreEqual(0, response.Count);
        }

        /// <summary>
        /// Tests whether the add contact call is successful.
        /// </summary>
        [Test]
        [Explicit]
        public static void CanAddContact()
        {
            var now = DateTime.Now;
            var email = "valmetcalf" + now.ToString("yyyyMMddHHmmss") + "@hotmail.com";
            var firstName = "Val" + now.ToString("yyyyMMddHHmmss");
            var lastName = "Metcalf";

            var handler = new ServiceCallHandler();
            var response = handler.AddContact(TestValues.InsightlyApiUrl, TestValues.InsightlyApiKey, email, firstName, lastName);

            Assert.IsNotNull(response);
            Assert.IsNotEmpty(response.ContactId);
            Assert.AreEqual("Metcalf", response.LastName);
        }

        /// <summary>
        /// Tests whether the add project link to contact call is successful.
        /// </summary>
        [Test]
        [Explicit]
        public static void CanAddProjectLinkToContact()
        {
            var handler = new ServiceCallHandler();
            var contacts = handler.GetContactsByEmail(TestValues.InsightlyApiUrl, TestValues.InsightlyApiKey, TestValues.Email);
            var response = handler.AddProjectLinkToContact(TestValues.InsightlyApiUrl, TestValues.InsightlyApiKey, contacts[0], TestValues.ProjectId);

            Assert.IsNotNull(response);
            Assert.IsTrue(response.Links.Length > 0);
        }

        /// <summary>
        /// Tests whether the add tags to contact call is successful.
        /// </summary>
        [Test]
        [Explicit]
        public static void CanAddTagsToContact()
        {
            var now = DateTime.Now;
            var tags = new StringCollection();

            tags.Add("A" + now.ToString("yyyyMMddHHmmss"));
            tags.Add("B" + now.ToString("yyyyMMddHHmmss"));

            var handler = new ServiceCallHandler();
            var contacts = handler.GetContactsByEmail(TestValues.InsightlyApiUrl, TestValues.InsightlyApiKey, TestValues.Email);
            var response = handler.AddTagsToContact(TestValues.InsightlyApiUrl, TestValues.InsightlyApiKey, contacts[0], tags);

            Assert.IsNotNull(response);
            Assert.IsTrue(response.Tags.Length > 0);
        }

        /// <summary>
        /// Tests whether the add note call is successful.
        /// </summary>
        [Test]
        [Explicit]
        public static void CanAddNote()
        {
            var now = DateTime.Now;
            var title = "Title: " + now.ToString("yyyyMMddHHmmss");
            var content = "Content: " + now.ToString("yyyyMMddHHmmss");

            var handler = new ServiceCallHandler();
            var response = handler.AddNote(
                TestValues.InsightlyApiUrl,
                TestValues.InsightlyApiKey,
                title,
                content,
                TestValues.ContactId,
                "Contact");

            Assert.IsNotNull(response);
            Assert.IsNotEmpty(response.NoteId);
            Assert.AreEqual(title, response.Title);
            Assert.AreEqual(content, response.Body);
            Assert.AreEqual(TestValues.ContactId, response.LinkSubjectId);
        }

        /// <summary>
        /// Tests whether the add note call with a complex body is successful.
        /// </summary>
        [Test]
        [Explicit]
        public static void CanAddNoteComplexBody()
        {
            var now = DateTime.Now;
            var nowText = now.ToString("yyyyMMddHHmmss");
            var title = "Title: " + nowText;
            var content = string.Format(@"SKU: {0} &#xD; Order Date: {1} &#xA; Order No.: {2} &#xD; Amount: {3} &#xA; Currency: {4}", nowText, nowText, nowText, nowText, nowText);

            Console.WriteLine(content);

            var handler = new ServiceCallHandler();
            var response = handler.AddNote(
                TestValues.InsightlyApiUrl,
                TestValues.InsightlyApiKey,
                title,
                content,
                TestValues.ContactId,
                "Contact");

            Console.WriteLine(response.Body);

            Assert.IsNotNull(response);
            Assert.IsNotEmpty(response.NoteId);
            Assert.AreEqual(title, response.Title);
            Assert.AreEqual(TestValues.ContactId, response.LinkSubjectId);
        }
    }
}