﻿//-------------------------------------------------
// <copyright file="TestValues.cs" company="Rapha">
//     Copyright (c) Rapha. All rights reserved.
// </copyright>
//-------------------------------------------------
namespace Rapha.Enterprise.TravelCrmService.NUnitTest
{
    /// <summary>
    /// The test constants.
    /// </summary>
    public class TestValues
    {
        /// <summary>
        /// The test contact ID.
        /// </summary>
        public const string ContactId = "137273633";

        /// <summary>
        /// The test email address.
        /// </summary>
        public const string Email = @"val.test.metcalf@hotmail.com";

        /// <summary>
        /// The Insightly API key.
        /// </summary>
        public const string InsightlyApiKey = "55c1b772-4e69-4675-ac88-83562ef76f24";

        /// <summary>
        /// The Insightly API URL.
        /// </summary>
        public const string InsightlyApiUrl = @"https://api.insight.ly/v2.1/";

        /// <summary>
        /// The test project ID.
        /// </summary>
        public const string ProjectId = "2580743";
    }
}
