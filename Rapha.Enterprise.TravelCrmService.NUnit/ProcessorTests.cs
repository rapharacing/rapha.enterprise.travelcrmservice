﻿//-----------------------------------------------------
// <copyright file="ProcessorTests.cs" company="Rapha">
//     Copyright (c) Rapha. All rights reserved.
// </copyright>
//-----------------------------------------------------
namespace Rapha.Enterprise.TravelCrmService.NUnitTest
{
    using System;

    using NUnit.Framework;

    /// <summary>
    /// The class containing the unit tests for the Processor class.
    /// </summary>
    [TestFixture]
    public class ProcessorTests
    {
        /// <summary>
        /// Tests whether the processing of orders is successful.
        /// </summary>
        [Test]
        public static void CanProcessOrders()
        {
            var processor = new Processor();
            var errors = processor.ProcessOrders();

            if (errors != null && errors.Count > 0)
            {
                foreach (var error in errors)
                {
                    Console.WriteLine(error);
                }
            }
        }
    }
}